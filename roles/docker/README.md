Docker
=========

A brief description of the role goes here.

Requirements
------------

This role only works for CentOS7 and installs and configure latest version of docker.

Role Variables
--------------

This role doesn't need any parameter.

Example Playbook
----------------

Just include the role without variables.

    - hosts: servers
      roles:
         - { role: docker }
