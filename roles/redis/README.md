Redis
=========

This role install and configure a Redis cluster with docker.

Requirements
------------

This role only works for CentOS7 and installs and configure latest stable version 4.X of redis.

Role Variables
--------------

This role accept a list of `redis_masters` and `redis_slaves`. This list should contain the port to configure in redis

        redis_masters: "{{ masters }}"
        redis_slaves: "{{ slaves }}"
        address: "{{ ip }}"

Dependencies
------------

This role needs to have installed and started Docker. Inside of this roles folder there's a docker role that can be used for this.

Example Playbook
----------------

```
    vars:
      ip: 192.168.77.21
      masters:
        - port: 7000
          status: master
      slaves:
        - port: 7001
          status: slave
        - port: 7002
          status: slave
    - hosts: servers
      roles:
        - { role: redis, redis_masters: "{{ masters }}", redis_slaves: "{{ slaves }}", address: "{{ ip }}" }
```
