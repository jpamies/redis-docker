export REDIS_VERSION:=4

ssh_config:
	vagrant ssh-config > ./ssh_config

dev: vagrantup

vagrantup:
	vagrant up

nodev:
	vagrant destroy

provision: ssh_config
	ansible-playbook -i hosts provision-infra.yml -vvv

configure-redis-cluster: configure-redis-masters

configure-redis-masters:
	echo yes|redis-cli --cluster create 192.168.77.21:7000 192.168.77.22:7000 192.168.77.23:7000
