Redis cluster
=========

Build a redundant Redis cluster in Docker.

## Requirements

- Redis should run on Docker
- Redis version 4.x, do not use Redis 5.x or 3.x
- Cluster should have HA
- Use ansible
- No need to tune it for performance or concurrency

## Solution

To test this solution we're going to use the following tools:

- Vagrant : https://www.vagrantup.com/downloads.html Vagrant will simulate the production environment creating VMs in our laptop.
- VirtualBox : is the engine used by Vagrant, VMWare should work too but it's untested.
- Ansible 2.7 : To provision and deploy the servers.
- Shell with `make` and `redis-cli` to interact with the solution implemented and test the cluster. Makefiles helps to automate all the steps and allow us to have a single entry point no matter if we run it in our laptop or CI/CD pipeline.

### Start DEV environment

Run `make dev` to setup 3 Application servers with Docker up and running.
Assuming 3 CentOS7 severs, you can check the details on ![Vagrantfile](Vagrantfile).

Vagrant will configure 3 servers on 192.168.77.21, 192.168.77.22 and 192.168.77.23.
Vagrant is executing the playbook ![provision-base-infra.yml](provision-base-infra.yml) applying the role ![docker](roles/docker/README.md).

Once this command finish, 3 servers will be ready to be used.

TIP: If you want to connect to the servers, running `make ssh_config` will create a `ssh_config` which can be used to ssh and check the docker containers manually.

### Installing Redis

To install Redis execute `make provision` will execute a playbook with the ![Redis](roles/redis/README.md) role.

The configuration prepared is adding 1 MASTER and 2 SLAVES of other MASTERS per Server. This allow us to stop 2 servers and still have a consistent cluster.

Notice that no MASTER & SLAVE of the same shard is configured on the same Server. That's why we're adding manually every slave to ensure the max HA possible.

```
 --------- CURRENT CONFIG --------
|    SERVER1: M1 S2 S3            |
|    SERVER2: S1 M2 S3            |
|    SERVER3: S1 S2 M3            |
 ---------------------------------
```


### Known issues

- This Redis role is installing and configuring Redis but also, it's configuring the cluster (master-slave relations). As the cluster configuration have no idempotent actions, this should be added to a separated role just for this.

- Redis role could be use to create a cluster of any size but, due the "hacky" config process it's only working for our current configuration. So this role can't be used for general cases.

### Tests

- Kill nodes and force the slave promotion meanwhile a script is writing.

- Run the redis-benchmark against the cluster.


### Further changes

- Use a container orchestrator or scheduler like Swarm or Nomad.

- Evaluate if the sharding is needed. Simple MASTER->SLAVEs configuration is easier to automate 100%. You may need haproxy or consul to hide this configuration from the application.


### LOGS - Create Servers and provision Docker

```
jpamies$ make dev
vagrant up
Bringing machine 'server1' up with 'virtualbox' provider...
Bringing machine 'server2' up with 'virtualbox' provider...
Bringing machine 'server3' up with 'virtualbox' provider...
==> server1: Importing base box 'centos/7'...
==> server1: Matching MAC address for NAT networking...
==> server1: Checking if box 'centos/7' is up to date...
==> server1: Setting the name of the VM: redis-cluster_server1_1544999542862_8926
==> server1: Vagrant has detected a configuration issue which exposes a
==> server1: vulnerability with the installed version of VirtualBox. The
==> server1: current guest is configured to use an E1000 NIC type for a
==> server1: network adapter which is vulnerable in this version of VirtualBox.
==> server1: Ensure the guest is trusted to use this configuration or update
==> server1: the NIC type using one of the methods below:
==> server1:
==> server1:   https://www.vagrantup.com/docs/virtualbox/configuration.html#default-nic-type
==> server1:   https://www.vagrantup.com/docs/virtualbox/networking.html#virtualbox-nic-type
==> server1: Clearing any previously set network interfaces...
==> server1: Preparing network interfaces based on configuration...
    server1: Adapter 1: nat
    server1: Adapter 2: hostonly
==> server1: Forwarding ports...
    server1: 22 (guest) => 2222 (host) (adapter 1)
==> server1: Booting VM...
==> server1: Waiting for machine to boot. This may take a few minutes...
    server1: SSH address: 127.0.0.1:2222
    server1: SSH username: vagrant
    server1: SSH auth method: private key
    server1:
    server1: Vagrant insecure key detected. Vagrant will automatically replace
    server1: this with a newly generated keypair for better security.
    server1:
    server1: Inserting generated public key within guest...
    server1: Removing insecure key from the guest if it's present...
    server1: Key inserted! Disconnecting and reconnecting using new SSH key...
==> server1: Machine booted and ready!
==> server1: Checking for guest additions in VM...
    server1: No guest additions were detected on the base box for this VM! Guest
    server1: additions are required for forwarded ports, shared folders, host only
    server1: networking, and more. If SSH fails on this machine, please install
    server1: the guest additions and repackage the box to continue.
    server1:
    server1: This is not an error message; everything may continue to work properly,
    server1: in which case you may ignore this message.
==> server1: Setting hostname...
==> server1: Configuring and enabling network interfaces...
==> server1: Rsyncing folder: /Users/jpamies/repos/redis-cluster/ => /vagrant
==> server2: Importing base box 'centos/7'...
==> server2: Matching MAC address for NAT networking...
==> server2: Checking if box 'centos/7' is up to date...
==> server2: Setting the name of the VM: redis-cluster_server2_1544999573458_98887
==> server2: Fixed port collision for 22 => 2222. Now on port 2200.
==> server2: Vagrant has detected a configuration issue which exposes a
==> server2: vulnerability with the installed version of VirtualBox. The
==> server2: current guest is configured to use an E1000 NIC type for a
==> server2: network adapter which is vulnerable in this version of VirtualBox.
==> server2: Ensure the guest is trusted to use this configuration or update
==> server2: the NIC type using one of the methods below:
==> server2:
==> server2:   https://www.vagrantup.com/docs/virtualbox/configuration.html#default-nic-type
==> server2:   https://www.vagrantup.com/docs/virtualbox/networking.html#virtualbox-nic-type
==> server2: Clearing any previously set network interfaces...
==> server2: Preparing network interfaces based on configuration...
    server2: Adapter 1: nat
    server2: Adapter 2: hostonly
==> server2: Forwarding ports...
    server2: 22 (guest) => 2200 (host) (adapter 1)
==> server2: Booting VM...
==> server2: Waiting for machine to boot. This may take a few minutes...
    server2: SSH address: 127.0.0.1:2200
    server2: SSH username: vagrant
    server2: SSH auth method: private key
    server2:
    server2: Vagrant insecure key detected. Vagrant will automatically replace
    server2: this with a newly generated keypair for better security.
    server2:
    server2: Inserting generated public key within guest...
    server2: Removing insecure key from the guest if it's present...
    server2: Key inserted! Disconnecting and reconnecting using new SSH key...
==> server2: Machine booted and ready!
==> server2: Checking for guest additions in VM...
    server2: No guest additions were detected on the base box for this VM! Guest
    server2: additions are required for forwarded ports, shared folders, host only
    server2: networking, and more. If SSH fails on this machine, please install
    server2: the guest additions and repackage the box to continue.
    server2:
    server2: This is not an error message; everything may continue to work properly,
    server2: in which case you may ignore this message.
==> server2: Setting hostname...
==> server2: Configuring and enabling network interfaces...
==> server2: Rsyncing folder: /Users/jpamies/repos/redis-cluster/ => /vagrant
==> server3: Importing base box 'centos/7'...
==> server3: Matching MAC address for NAT networking...
==> server3: Checking if box 'centos/7' is up to date...
==> server3: Setting the name of the VM: redis-cluster_server3_1544999603341_19622
==> server3: Fixed port collision for 22 => 2222. Now on port 2201.
==> server3: Vagrant has detected a configuration issue which exposes a
==> server3: vulnerability with the installed version of VirtualBox. The
==> server3: current guest is configured to use an E1000 NIC type for a
==> server3: network adapter which is vulnerable in this version of VirtualBox.
==> server3: Ensure the guest is trusted to use this configuration or update
==> server3: the NIC type using one of the methods below:
==> server3:
==> server3:   https://www.vagrantup.com/docs/virtualbox/configuration.html#default-nic-type
==> server3:   https://www.vagrantup.com/docs/virtualbox/networking.html#virtualbox-nic-type
==> server3: Clearing any previously set network interfaces...
==> server3: Preparing network interfaces based on configuration...
    server3: Adapter 1: nat
    server3: Adapter 2: hostonly
==> server3: Forwarding ports...
    server3: 22 (guest) => 2201 (host) (adapter 1)
==> server3: Booting VM...
==> server3: Waiting for machine to boot. This may take a few minutes...
    server3: SSH address: 127.0.0.1:2201
    server3: SSH username: vagrant
    server3: SSH auth method: private key
    server3:
    server3: Vagrant insecure key detected. Vagrant will automatically replace
    server3: this with a newly generated keypair for better security.
    server3:
    server3: Inserting generated public key within guest...
    server3: Removing insecure key from the guest if it's present...
    server3: Key inserted! Disconnecting and reconnecting using new SSH key...
==> server3: Machine booted and ready!
==> server3: Checking for guest additions in VM...
    server3: No guest additions were detected on the base box for this VM! Guest
    server3: additions are required for forwarded ports, shared folders, host only
    server3: networking, and more. If SSH fails on this machine, please install
    server3: the guest additions and repackage the box to continue.
    server3:
    server3: This is not an error message; everything may continue to work properly,
    server3: in which case you may ignore this message.
==> server3: Setting hostname...
==> server3: Configuring and enabling network interfaces...
==> server3: Rsyncing folder: /Users/jpamies/repos/redis-cluster/ => /vagrant
==> server3: Running provisioner: ansible...
Vagrant gathered an unknown Ansible version:


and falls back on the compatibility mode '1.8'.

Alternatively, the compatibility mode can be specified in your Vagrantfile:
https://www.vagrantup.com/docs/provisioning/ansible_common.html#compatibility_mode

PLAY [all] *********************************************************************

TASK [Gathering Facts] *********************************************************
ok: [server3]
ok: [server2]
ok: [server1]

TASK [docker : docker repo] ****************************************************
changed: [server2]
changed: [server3]
changed: [server1]

TASK [docker : remove previous version of docker] ******************************
ok: [server3]
ok: [server2]
ok: [server1]

TASK [docker : ensure docker is at the latest version] *************************
changed: [server3]
changed: [server1]
changed: [server2]

TASK [docker : enable docker] **************************************************
changed: [server1]
changed: [server3]
changed: [server2]

PLAY RECAP *********************************************************************
server1                    : ok=5    changed=3    unreachable=0    failed=0
server2                    : ok=5    changed=3    unreachable=0    failed=0
server3                    : ok=5    changed=3    unreachable=0    failed=0
```

### LOGS - Provision REDIS cluster

```
jpamies$ make provision
ansible-playbook -i hosts provision-infra.yml

PLAY [all] *************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *************************************************************************************************************************************************************************************************
ok: [server1]
ok: [server3]
ok: [server2]

TASK [redis : Create redis Data folder] ********************************************************************************************************************************************************************************
changed: [server1] => (item={u'status': u'slave', u'port': 7001})
changed: [server2] => (item={u'status': u'slave', u'port': 7001})
changed: [server3] => (item={u'status': u'slave', u'port': 7001})
changed: [server3] => (item={u'status': u'slave', u'port': 7002})
changed: [server1] => (item={u'status': u'slave', u'port': 7002})
changed: [server2] => (item={u'status': u'slave', u'port': 7002})
changed: [server1] => (item={u'status': u'master', u'port': 7000})
changed: [server3] => (item={u'status': u'master', u'port': 7000})
changed: [server2] => (item={u'status': u'master', u'port': 7000})

TASK [redis : Create redis configuration] ******************************************************************************************************************************************************************************
changed: [server3] => (item={u'status': u'slave', u'port': 7001})
changed: [server1] => (item={u'status': u'slave', u'port': 7001})
changed: [server2] => (item={u'status': u'slave', u'port': 7001})
changed: [server3] => (item={u'status': u'slave', u'port': 7002})
changed: [server1] => (item={u'status': u'slave', u'port': 7002})
changed: [server2] => (item={u'status': u'slave', u'port': 7002})
changed: [server3] => (item={u'status': u'master', u'port': 7000})
changed: [server1] => (item={u'status': u'master', u'port': 7000})
changed: [server2] => (item={u'status': u'master', u'port': 7000})

TASK [redis : Setup redis containers] **********************************************************************************************************************************************************************************
changed: [server2] => (item={u'status': u'slave', u'port': 7001})
changed: [server3] => (item={u'status': u'slave', u'port': 7001})
changed: [server1] => (item={u'status': u'slave', u'port': 7001})
changed: [server2] => (item={u'status': u'slave', u'port': 7002})
changed: [server3] => (item={u'status': u'slave', u'port': 7002})
changed: [server1] => (item={u'status': u'slave', u'port': 7002})
changed: [server2] => (item={u'status': u'master', u'port': 7000})
changed: [server3] => (item={u'status': u'master', u'port': 7000})
changed: [server1] => (item={u'status': u'master', u'port': 7000})

TASK [redis : Pull redis-cli 5 to prevent errors] **********************************************************************************************************************************************************************
changed: [server1]
changed: [server2]
changed: [server3]

TASK [redis : Create cluster using redis-cli 5] ************************************************************************************************************************************************************************
changed: [server1]

TASK [redis : Get other masters] ***************************************************************************************************************************************************************************************
changed: [server1]
changed: [server2]
changed: [server3]

TASK [redis : set_fact] ************************************************************************************************************************************************************************************************
ok: [server1]
ok: [server2]
ok: [server3]

TASK [redis : debug] ***************************************************************************************************************************************************************************************************
ok: [server1] => (item=0) => {
    "msg": "docker run -ti redis:5 sh -c 'redis-cli --cluster add-node 192.168.77.21:7001 192.168.77.21:7000 --cluster-slave --cluster-master-id 1b95b1562b60ff607208d306f2cc9e110cdbe7ad'"
}
ok: [server1] => (item=1) => {
    "msg": "docker run -ti redis:5 sh -c 'redis-cli --cluster add-node 192.168.77.21:7002 192.168.77.21:7000 --cluster-slave --cluster-master-id 9dfa3206ff4330949a2ad672533acf3d28bd584f'"
}
ok: [server2] => (item=0) => {
    "msg": "docker run -ti redis:5 sh -c 'redis-cli --cluster add-node 192.168.77.22:7001 192.168.77.22:7000 --cluster-slave --cluster-master-id 1b95b1562b60ff607208d306f2cc9e110cdbe7ad'"
}
ok: [server2] => (item=1) => {
    "msg": "docker run -ti redis:5 sh -c 'redis-cli --cluster add-node 192.168.77.22:7002 192.168.77.22:7000 --cluster-slave --cluster-master-id 4fec579cc4e70668d82dfd440cc02c26964ae7f4'"
}
ok: [server3] => (item=0) => {
    "msg": "docker run -ti redis:5 sh -c 'redis-cli --cluster add-node 192.168.77.23:7001 192.168.77.23:7000 --cluster-slave --cluster-master-id 9dfa3206ff4330949a2ad672533acf3d28bd584f'"
}
ok: [server3] => (item=1) => {
    "msg": "docker run -ti redis:5 sh -c 'redis-cli --cluster add-node 192.168.77.23:7002 192.168.77.23:7000 --cluster-slave --cluster-master-id 4fec579cc4e70668d82dfd440cc02c26964ae7f4'"
}

TASK [redis : Connect slaves] ******************************************************************************************************************************************************************************************
changed: [server2] => (item=0)
changed: [server3] => (item=0)
changed: [server1] => (item=0)
changed: [server3] => (item=1)
changed: [server2] => (item=1)
changed: [server1] => (item=1)

PLAY RECAP *************************************************************************************************************************************************************************************************************
server1                    : ok=10   changed=7    unreachable=0    failed=0
server2                    : ok=9    changed=6    unreachable=0    failed=0
server3                    : ok=9    changed=6    unreachable=0    failed=0
```

Cluster status after inserting a few keys:
```
jpamies$ redis-cli --cluster info  192.168.77.23:7000
192.168.77.23:7000 (1b95b156...) -> 1 keys | 5461 slots | 2 slaves.
192.168.77.21:7000 (4fec579c...) -> 4 keys | 5461 slots | 2 slaves.
192.168.77.22:7000 (9dfa3206...) -> 6 keys | 5462 slots | 2 slaves.
[OK] 11 keys in 3 masters.
```
